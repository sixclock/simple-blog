from flask_sqlalchemy import SQLAlchemy
from datetime import datetime, date

db = SQLAlchemy()

class User(db.Model):
    __tablename__ = "t_user"

    ID = db.Column(db.Integer, primary_key=True, autoincrement=True)
    createTime = db.Column(db.DateTime, default=datetime.now)
    updateTime = db.Column(db.DateTime, default=datetime.now, onupdate=datetime.now)
    # 账号
    account = db.Column(db.String(80))
    # 密码
    password = db.Column(db.String(80))
    # 昵称
    nickname = db.Column(db.String(80))
    # 简介
    introduction = db.Column(db.String(200))
    # 头像
    avatar = db.Column(db.String(100))
    # 性别，1男、2女、3保密
    gender = db.Column(db.Integer, default=3)


class Blog(db.Model):
    __tablename__ = "t_blog"

    ID = db.Column(db.Integer, primary_key=True, autoincrement=True)
    createTime = db.Column(db.DateTime, default=datetime.now)
    updateTime = db.Column(db.DateTime, default=datetime.now, onupdate=datetime.now)

    # 作者
    userID = db.Column(db.Integer)
    # 标题
    title = db.Column(db.String(200))
    # 内容
    content = db.Column(db.Text())
    # 封面
    cover = db.Column(db.String(200))
    # 阅读数量
    readCount = db.Column(db.Integer)
    # 状态，0未发布、1已发布
    status = db.Column(db.Integer)

