from exts.db import db
from exts.cors import cors

def initialized(app):
    db.init_app(app)
    cors.init_app(app)
