from flask.cli import AppGroup
from exts.db import *

password_cli = AppGroup("password")

@password_cli.command("reset")
def reset():
    account = input("账号：")
    password = input("密码：")

    user = db.session.query(User).filter(
        User.account == account
    ).update({
        User.password: password
    })
    db.session.commit()
