from commands.initialized import initialized_cli
from commands.password import password_cli

def register(app):
    app.cli.add_command(initialized_cli)
    app.cli.add_command(password_cli)
