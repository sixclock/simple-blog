from flask.cli import AppGroup
from exts.db import *

initialized_cli = AppGroup("initialized")

@initialized_cli.command("db")
def initialized_db():
    db.create_all()

@initialized_cli.command("create-admin")
def create_admin():
    account = input("账号：")
    password = input("密码：")
    nickname = input("昵称：")

    user = User(
        account=account,
        password=password,
        nickname=nickname
    )
    db.session.add(user)
    db.session.commit()
