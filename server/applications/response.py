from flask import jsonify

class ResponseCode:

    Success = 0
    Error = 1000

    DataNotExist = 2001


class Response:

    @staticmethod
    def success(data=None):
        if data is None:
            data = {}
        return jsonify({
            "code": ResponseCode.Success,
            "msg": "请求成功！",
            "data": data
        })

    @staticmethod
    def error(code=ResponseCode.Error, msg="请求异常！"):
        return jsonify({
            "code": code,
            "msg": msg
        })
