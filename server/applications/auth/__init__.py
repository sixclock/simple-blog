from flask import Blueprint, jsonify
from flask import request

auth_app = Blueprint("auth", __name__)

@auth_app.route("/login", methods=["POST"])
def login():
    """
    登录
    :return:
    """
    params = request.json
    account = params.get("account")
    password = params.get("password")
    return jsonify({
        "code": 0,
        "data": {
            "nickname": "hahaha",
            "avatar": "https://cube.elemecdn.com/3/7c/3ea6beec64369c2642b92c6726f1epng.png"
        }
    })

@auth_app.route("/logout", methods=["POST"])
def logout():
    """
    登出
    :return:
    """
    pass

@auth_app.route("/user/info", methods=["GET"])
def user_info():
    return jsonify({
        "code": 0,
        "data": {
            "nickname": "hahaha",
            "avatar": "https://cube.elemecdn.com/3/7c/3ea6beec64369c2642b92c6726f1epng.png"
        }
    })