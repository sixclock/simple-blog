from flask import Flask
import tomllib

import commands
import exts

# 读取 config.toml 中的配置
with open("config.toml", "rb") as fp:
    config = tomllib.load(fp)

def create_app():
    app = Flask(__name__)

    # 加载配置
    app.config.from_mapping(config)

    # 初始化插件
    exts.initialized(app)

    # 注册命令行工具
    commands.register(app)

    # 注册蓝图
    from applications.auth import auth_app
    app.register_blueprint(auth_app, url_prefix="/auth")
    from applications.blog import blog_app
    app.register_blueprint(blog_app, url_prefix="/blog")

    return app
