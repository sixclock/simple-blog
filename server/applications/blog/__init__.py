import random

from flask import Blueprint, request, jsonify

from applications.response import Response, ResponseCode
from exts import db
from exts.db import Blog

blog_app = Blueprint("blog", __name__)

@blog_app.route("/create", methods=["POST"])
def create():
    """
    创建文章
    :return:
    """
    params = request.json
    title = params.get("title")
    newBlog = Blog(
        title=title
    )
    db.session.add(newBlog)
    db.session.commit()
    return Response.success()

@blog_app.route("/list", methods=["GET"])
def list():
    """
    文章列表
    :return:
    """
    params = request.args
    page = int(params.get("page", 1))
    limit = int(params.get("limit", 20))

    blogQuery = db.session.query(
        Blog
    ).filter(
        Blog.status == 1
    ).order_by(
        Blog.createTime.desc()
    ).paginate(
        page=page,
        per_page=limit
    )
    data = {
        "list": [],
        "total": blogQuery.total
    }
    for item in blogQuery:
        data["list"].append({
            "title": item.title,
            "introduce": "",
            "readCount": item.readCount,
            "createTime": item.createTime
        })
    return Response.success(data)

@blog_app.route("/detail", methods=["GET"])
def detail():
    """
    文章详情
    :return:
    """
    params = request.args
    blogID = params.get("blogID")

    blogQuery = db.sessioin.query(Blog).filter(
        Blog.ID == blogID
    ).first()
    if not blogQuery:
        return Response.error(ResponseCode.DataNotExist, "博客不存在！")
    data = {
        "title": blogQuery.title,
        "content": blogQuery.content
    }
    return Response.success(data)

@blog_app.route("/update", methods=["POST"])
def update():
    """
    更新文章
    :return:
    """
    params = request.json
    title = params.get("title")
    content = params.get("content")
    blogID = params.get("blogID")

    blogQuery = db.session.query(Blog).filter(
        Blog.ID == blogID
    ).first()

    blogQuery.title = title
    blogQuery.content = content

    db.session.commit()
    return Response.success()


@blog_app.route("/delete", methods=["POST"])
def delete():
    """
    删除文章
    :return:
    """
    pass

@blog_app.route("/release", methods=["POST"])
def release():
    """
    发布文章
    :return:
    """
    pass
