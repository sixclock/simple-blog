# SimpleBlog

一个简单的博客示例，使用Flask+Vue3实现

## 实现功能

- 内容发布与管理：
    - 文章发布：可以创建、编辑和发布博客文章。
    - 文章列表管理：对所有文章进行草稿和发布。

## 后端

- Flask

## 前端

- Vue3

使用markdown编辑器：`https://github.com/imzbf/md-editor-v3`