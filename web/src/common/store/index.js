import { createStore } from 'vuex';
import http from '../http.js';

export default createStore({
    state: {
        isLogin: false,
        userInfo: {}
    },
    mutations: {
        // 更新登录状态
        SET_LOGIN_STATUS(state, status) {
            state.isLogin = status;
        },
        // 更新用户信息
        SET_USER_INFO(state, userInfo) {
            state.userInfo = userInfo;
        }
    },
    actions: {
        // 登录动作
        login({ commit }, userInfo) {
            commit('SET_LOGIN_STATUS', true);
            commit('SET_USER_INFO', userInfo);
        },
        // 登出动作
        logout({ commit }) {
            commit('SET_LOGIN_STATUS', false);
            commit('SET_USER_INFO', {});
        }
    },
    getters: {
        // 获取登录状态
        isLogin: state => state.isLogin,
        // 获取用户信息
        userInfo: state => state.userInfo
    }
});
