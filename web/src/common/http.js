import axios from 'axios';

const http = axios.create({
    baseURL: 'http://127.0.0.1:5000/', // 设置请求根地址
    timeout: 60000, // 设置请求超时时间
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    },
    withCredentials: true
});

// 请求拦截器
http.interceptors.request.use(
    config => {
        // 在发送请求之前做些什么
        // 例如，如果有token，你可以在这里统一设置
        // config.headers['X-Token'] = '你的token';
        return config;
    },
    error => {
        // 对请求错误做些什么
        return Promise.reject(error);
    }
);

// 响应拦截器
http.interceptors.response.use(
    response => {
        // 对响应数据做点什么
        let res = response.data;
        // 例如，根据返回的状态码判断请求是否成功
        if (res.code !== 0) {
            // 业务错误处理，比如弹窗提示等
            return Promise.reject(new Error(res.message || 'Error'));
        } else {
            return res;
        }
    },
    error => {
        // 对响应错误做点什么
        return Promise.reject(error);
    }
);

export default http;
